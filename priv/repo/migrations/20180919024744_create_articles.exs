defmodule Goodreaders.Repo.Migrations.CreateArticles do
  use Ecto.Migration

  def change do
    create table(:articles) do
      add :title, :string
      add :content, :string
      add :author, :string

      timestamps()
    end

    create index(:articles, [:author])
  end
end
